$TTL    604800
@				IN	SOA	dns1.d4rk.io. dnsadmin.d4rk.io. (2020031201 604800 86400 2419200 604800)
@				IN	NS	dns1.d4rk.io.

; === Server ===
u18-de-fa-02.compute     	IN      A       159.69.242.205
u18-de-fa-02.compute     	IN      AAAA    2a01:4f8:c010:22ca::1
u18-de-fa-03.compute     	IN      A       159.69.245.232
u18-de-fa-03.compute     	IN      AAAA    2a01:4f8:1c17:7fc4::1
n1.compute               	IN      A       195.201.233.46
n1.compute               	IN      AAAA    2a01:4f8:c010:3c90::1

; === Services ===
; dns
dns1				IN 	CNAME	n1.compute.d4rk.io.	
; teamspeak
ts3				IN	A	159.69.245.232
ts3				IN	AAAA	2a01:4f8:1c17:7fc4::1
ts				IN      A       159.69.245.232
ts				IN      AAAA    2a01:4f8:1c17:7fc4::1
teamspeak			IN	CNAME	u18-de-fa-03.compute.d4rk.io.
; web server
mailer                   	IN      CNAME   u18-de-fa-02.compute.d4rk.io.
chat                            IN      CNAME   u18-de-fa-03.compute.d4rk.io.
git                      	IN      CNAME   u18-de-fa-03.compute.d4rk.io.
; mail
mail                     	IN      CNAME   u18-de-fa-02.compute.d4rk.io.
imap                     	IN      CNAME   mail.d4rk.io.
smtp                    	IN      CNAME   mail.d4rk.io.
d4rk.io.                 	IN      MX    0 mail.d4rk.io.
d4rk.io.            	3600 	IN      TXT     "v=spf1 mx -all"
key1._domainkey.d4rk.io. 	IN      TXT     "v=DKIM1; h=sha256; k=rsa; " "p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp2+X7MEDyWWYghkj/JH7LCSLbvtpMMMxjYMGNR+Oq7NobzrGOOmzPAb/RGA2fFJO0Cns9dTyczCZaE5t3M9U+wtUMWdkHBbJjVKfKtxmtxIzU4GiCAJG6l2ZMDZ9mhprOmLViZLq0MLC7WCUpj2c75/P9LIkcCnDZs+Gqnfil5ahqUYNELkxOlQ+QComXaeQW/kkxg3HLfXr/I" "Q/lXUo2WjNvnzDyXAAp2m8Zbk8lt0eBWWlP0GDZCs16tpvDwRYLzSak71SGzxdJn9TnOaDeTSaRnLdefeXpdvaGh8TxGPnq4XdgmY1HhtWK8Gfhf3Phrrby4AAAnHdcntEIzDDPQIDAQAB"
